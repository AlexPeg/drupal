# SITE DRUPAL

### Prérequis

- Installation de [Composer](https://getcomposer.org/doc/00-intro.md#installation-linux-unix-osx)
- Installation de [Drush Launcher](https://github.com/drush-ops/drush-launcher)

### Installation en local

- Cloner le projet et installer les dépendances :

```
git clone git@gitlab.com:AlexPeg/drupal.git
cd drupal
composer install
```
- Lancer le serveur dans le dossier "web":

```
cd web
php -S 0.0.0.0:8000
```
- Dans PHPMyAdmin, créer une base de données pour ce projet en utf9mb4_unicode_ci

- Aller sur localhost:8000 et suivre les instructions d'installation en renseignant le nom de la base de données qu'on vient de créer ainsi que son identifiant et mot de passe PHPMyAdmin

- Vous pouvez arrêter l'installation à l'étape "Configurer le site"

- Importer la base de données :

```
drush sql-cli < data/drupal.sql
```

- Vous pouvez maintenant retourner sur localhost:8000 et vous identifier avec le couple identifiant/mot de passe super sécurisé admin/admin! 

