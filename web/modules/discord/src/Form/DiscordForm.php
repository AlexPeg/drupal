<?php

namespace Drupal\discord\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a discord form.
 */
class DiscordForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'discord_discord';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['message'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Message'),
      '#required' => TRUE,
    ];

    $form['actions'] = [
      '#type' => 'actions',
    ];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Send'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if (mb_strlen($form_state->getValue('message')) < 10) {
      $form_state->setErrorByName('name', $this->t('Message should be at least 10 characters.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $message = $form_state->getValue('message');
    $url = 'https://discord.com/api/webhooks/816964946880626688/aHfkIfwVm6liVVbsVbRGe_lHQa77o3YpRSECmCPJOq8dD75boZdF72HMbpPpHwoM_fp3';// Adresse de votre webhook
    $data = array(
      'content' => $message, 
    );
    $context = array(
      'http' => array(
        'method' => 'POST',
        'header' => "Content-type: application/json\r\n",
        'content' => json_encode($data),
      )
    );
    /*
     * Attention, certains serveurs désactivent la fonction 'allow_url_fopen'.
     * Si c'est votre cas et si vous ne pouvez pas l'activer, vous ne pourrez
     * pas utiliser ce script directement. Vous devrez utiliser cURL !
     */
    $context  = stream_context_create($context);
    $result = @file_get_contents($url, false, $context);
    if($result === false) {
      $this->messenger()->addStatus($this->t('ERREUR'));
    }
    $this->messenger()->addStatus($this->t('The message has been sent.'));
  }

}
