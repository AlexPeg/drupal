<?php

namespace Drupal\discord\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Returns responses for Discord routes.
 */
class DiscordController extends ControllerBase {

  /**
   * Builds the response.
   */
  public function build() {

    $build['content'] = [
      '#type' => 'item',
      '#markup' => $this->t('It works!'),
    ];

    return $build;
  }

}
